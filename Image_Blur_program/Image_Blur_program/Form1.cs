﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Windows.Forms;
using System.Drawing.Imaging;
using System.Threading.Tasks;

namespace Image_Blur_program
{
    public partial class Form1 : Form
    {
        // Declare variables 
        string myFile = "";
        string savedFile = "";
        Image File;
        Boolean fileOpen;
        Bitmap newBitMap;
        BitmapData bmpData;
        int blurAmount = 1;
        /*Thread q1;
        Thread q2;
        Thread q3;
        Thread q4;*/
        private int heightlessblur = 0;
        private int widthlessblur =0;
        Bitmap cloneBitmap;
        Bitmap myBitmap;
        Bitmap firstHalf;
        Bitmap secondHalf;
        //object thislock = new object();


        public Form1()
        {
            InitializeComponent();
            
            label1.Visible = false;
            txtBlurLvl.Visible = false;
            pictureBoxImage.Visible = false;
            btnProcess.Visible = false;
            trackBar1.Visible = false;
            //groupBox3.Visible = false;
        }

        public void cloneImage()
        {
            myBitmap = new Bitmap(myFile);
            Rectangle cloneRect = new Rectangle(0, 0, myBitmap.Width / 2, myBitmap.Height);
            firstHalf = myBitmap.Clone(cloneRect, myBitmap.PixelFormat);
           // firstHalf.Save("C:/Users/Armee/Desktop");
            PixelFormat format = newBitMap.PixelFormat;



            Bitmap cloneBitmap = newBitMap.Clone(cloneRect, format);
            Rectangle rect = new Rectangle(myBitmap.Width / 2, 0, myBitmap.Width / 2, myBitmap.Height);
            secondHalf = myBitmap.Clone(rect, cloneBitmap.PixelFormat);
            //e.Graphics.DrawImage(cloneBitmap, 0, 0);
        }

    

        public void quadrant1()
        {
            
            //object thislock1 = new object();
           // lock (thislock1)
           // {
                    //top left of picture 
                    for (int x = blurAmount; x < (widthlessblur / 2); x++)
                    {
                        for (int y = blurAmount; y < (heightlessblur / 2); y++)
                        {
                            try
                            {

                                Color prevX = newBitMap.GetPixel(x - blurAmount, y);
                                Color prevY = newBitMap.GetPixel(x, y - blurAmount);
                                Color nextX = newBitMap.GetPixel(x + blurAmount, y);
                                Color nextY = newBitMap.GetPixel(x, y + blurAmount);



                                int avgR = (int)(prevX.R + nextX.R + prevY.R + nextY.R) / 4;
                                int avgG = (int)(prevX.G + nextX.G + prevY.G + nextY.G) / 4;
                                int avgB = (int)(prevX.B + nextX.B + prevY.B + nextY.B) / 4;

                                firstHalf.SetPixel(x, y, Color.FromArgb(avgR, avgG, avgB));
                                
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show(ex.Message);
                            }

                        }
                    }
            // }
        }

        /*public void lockThread()
        {

            Rectangle rect = new Rectangle(0, 0, newBitMap.Width, newBitMap.Height);
            bmpData = newBitMap.LockBits(rect, ImageLockMode.ReadWrite, newBitMap.PixelFormat);
            IntPtr ptr = bmpData.Scan0;
        }*/

        public void quadrant2()
        {;
            //object thislock2 = new object();
           // lock (thislock2)
           // {
                    //top right of picture
                    for (int x = blurAmount; x < (((widthlessblur) * 2) / 4); x++)
                    {
                        for (int y = blurAmount; y < (heightlessblur) / 2; y++)
                        {
                            try
                            {

                                Color prevX = newBitMap.GetPixel(x - blurAmount, y);
                                Color prevY = newBitMap.GetPixel(x, y - blurAmount);
                                Color nextX = newBitMap.GetPixel(x + blurAmount, y);
                                Color nextY = newBitMap.GetPixel(x, y + blurAmount);


                                int avgR = (int)(prevX.R + nextX.R + prevY.R + nextY.R) / 4;
                                int avgG = (int)(prevX.G + nextX.G + prevY.G + nextY.G) / 4;
                                int avgB = (int)(prevX.B + nextX.B + prevY.B + nextY.B) / 4;

                                newBitMap.SetPixel(x, y, Color.FromArgb(avgR, avgG, avgB));




                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show(ex.Message);
                            }

                        }
                    }
                    
               // }
        }

        
        public void quadrant3()
        {
            object thislock3 = new object();
            lock (thislock3)
            {
                    //bottom left
                    for (int x = blurAmount; x < (widthlessblur / 2); x++)
                    {
                        for (int y = blurAmount; y < ((heightlessblur * 2) / 4); y++)
                        {
                            try
                            {

                                Color prevX = newBitMap.GetPixel(x - blurAmount, y);
                                Color prevY = newBitMap.GetPixel(x, y - blurAmount);
                                Color nextX = newBitMap.GetPixel(x + blurAmount, y);
                                Color nextY = newBitMap.GetPixel(x, y + blurAmount);


                                int avgR = (int)(prevX.R + nextX.R + prevY.R + nextY.R) / 4;
                                int avgG = (int)(prevX.G + nextX.G + prevY.G + nextY.G) / 4;
                                int avgB = (int)(prevX.B + nextX.B + prevY.B + nextY.B) / 4;

                                newBitMap.SetPixel(x, y, Color.FromArgb(avgR, avgG, avgB));




                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show(ex.Message);
                            }
                        }

                    }
               }
        }

        public void quadrant4()
        {
            object thislock4 = new object();
            lock (thislock4)
            {
                    //bottom right
                    for (int x = blurAmount; x < ((widthlessblur * 2) / 4); x++)
                    {
                        for (int y = blurAmount; y < ((heightlessblur * 2) / 4); y++)
                        {
                            try
                            {

                                Color prevX = newBitMap.GetPixel(x - blurAmount, y);
                                Color prevY = newBitMap.GetPixel(x, y - blurAmount);
                                Color nextX = newBitMap.GetPixel(x + blurAmount, y);
                                Color nextY = newBitMap.GetPixel(x, y + blurAmount);

                                int avgR = (int)(prevX.R + nextX.R + prevY.R + nextY.R) / 4;
                                int avgG = (int)(prevX.G + nextX.G + prevY.G + nextY.G) / 4;
                                int avgB = (int)(prevX.B + nextX.B + prevY.B + nextY.B) / 4;

                                newBitMap.SetPixel(x, y, Color.FromArgb(avgR, avgG, avgB));
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show(ex.Message);
                            }

                        }
                    }
                }
        }

       

        public void runSingleThreadMultipleTimes()
        {
            heightlessblur = newBitMap.Height - blurAmount;
            widthlessblur = newBitMap.Width - blurAmount;


            if (rBtnThread.Checked)
            {
                groupBox3.Visible = true;

                if (rBtnOne.Checked)
                {
                    Thread q1 = new Thread(new ThreadStart(Process));
                    q1.Start();
                }

                else if (rBtnTwo.Checked)
                {
                    Thread q1 = new Thread(new ThreadStart(quadrant1));
                    q1.Start();
                    Thread q2 = new Thread(new ThreadStart(quadrant2));
                    q2.Start();
                }

            }




            /*
                            try
                            {
                                q1 = new Thread(new ThreadStart(quadrant1));
                                q2 = new Thread(new ThreadStart(quadrant2));
                                q3 = new Thread(new ThreadStart(quadrant3));
                                q4 = new Thread(new ThreadStart(quadrant4));

                                for (int i = 0; i< 10; i++)
                                {


                                }

                                pictureBoxImage.SizeMode = PictureBoxSizeMode.StretchImage;
                                pictureBoxImage.Image = newBitMap;
                                saveFileDialog1.InitialDirectory = @"c:\";
                                if (saveFileDialog1.ShowDialog() == DialogResult.OK)
                                {
                                    savedFile = saveFileDialog1.FileName + ".png";
                                }
                                newBitMap.Save(@savedFile);
                            }
                            catch(Exception ex)
                            {
                                MessageBox.Show(ex.Message);
                            }*/




            if (rBtnProcess.Checked)
            {
                Process();
            }

            MessageBox.Show("Blur effect complete with Blur effect level " + txtBlurLvl.Text + "\n Saved Location: " + savedFile, "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        public void Process()
        {
            for (int x = blurAmount; x < newBitMap.Width - blurAmount; x++)
            {
                for (int y = blurAmount; y < newBitMap.Height - blurAmount; y++)
                {
                    try
                    {

                        Color prevX = newBitMap.GetPixel(x - blurAmount, y);
                        Color prevY = newBitMap.GetPixel(x, y - blurAmount);
                        Color nextX = newBitMap.GetPixel(x + blurAmount, y);
                        Color nextY = newBitMap.GetPixel(x, y + blurAmount);
                        Color topLeft = newBitMap.GetPixel(x - 1, y + 1);
                        Color topRight = newBitMap.GetPixel(x + 1, y + 1);
                        Color btmLeft = newBitMap.GetPixel(x - 1, y - 1);
                        Color btmRight = newBitMap.GetPixel(x + 1, y - 1);


                        int avgR = ((prevX.R + nextX.R + prevY.R + nextY.R + topLeft.R + topRight.R + btmLeft.R + btmRight.R)) / 9;
                        int avgG = ((prevX.G + nextX.G + prevY.G + nextY.G + topLeft.G + topRight.G + btmLeft.G + btmRight.G)) / 9;
                        int avgB = ((prevX.B + nextX.B + prevY.B + nextY.B + topLeft.B + topRight.B + btmLeft.B + btmRight.B)) / 9;
                        

                        newBitMap.SetPixel(x, y, Color.FromArgb(avgR, avgG, avgB));
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }


                }

            }
            pictureBoxImage.SizeMode = PictureBoxSizeMode.StretchImage;
            pictureBoxImage.Image = newBitMap;

            saveFileDialog1.InitialDirectory = @"c:\";
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                savedFile = saveFileDialog1.FileName + ".png";
            }
            newBitMap.Save(@savedFile);
        }




        private void btnProcess_Click(object sender, EventArgs e)
        {
            //if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            //{
            runSingleThreadMultipleTimes();
                //cloneImage();
                
            //}
            //firstHalf.Save(@savedFile);
            newBitMap.Save(@savedFile);
            MessageBox.Show("test");



        }





















        private void btnBrowse_Click(object sender, EventArgs e)
        {
            //specifiy initial directory
            openFileDialog1.InitialDirectory = @"c:\";

            //opens file dialog and allows you to browse to the specified image and after selecting it, it loads it in the pictureBox.
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                myFile = openFileDialog1.FileName;
                newBitMap = new Bitmap(myFile);
                txtBrowseLocation.Text = myFile;
                pictureBoxImage.Image = Image.FromFile(@myFile);
                
                label1.Visible = true;
                txtBlurLvl.Visible = true;
                pictureBoxImage.Visible = true;
                btnProcess.Visible = true;
                trackBar1.Visible = true;
                blurAmount = int.Parse(trackBar1.Value.ToString());
                txtBlurLvl.Text = Convert.ToString(blurAmount);
            }
        }

        private void trackBar1_Scroll(object sender, EventArgs e)
        {
            blurAmount = int.Parse(trackBar1.Value.ToString());
            txtBlurLvl.Text = Convert.ToString(blurAmount);
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            pictureBoxImage.Visible = false; 

            txtBlurLvl.Clear();

            txtBrowseLocation.Clear();
        
            // clears the contents on the form 


        }

    }
}

